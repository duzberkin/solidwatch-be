// DB connection config file (connection info)
const { Sequelize } = require("sequelize");

let db = new Sequelize("postgres", "solid", "solid123", {
  host: "db-solid.cdkir9lmtvkr.us-east-1.rds.amazonaws.com",
  port: 5432,
  dialect: "postgres",
});

db.authenticate()
  .then(() => {
    console.log("Connection has been established successfully.");
  })
  .catch((err) => {
    console.log("Unable to connect to the database:", err);
  });

module.exports = db;
