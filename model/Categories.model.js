const { Sequelize } = require("sequelize");
const db = require("../config/db.config");

// Categories model
const Categories = db.define("categories", {
  categoryName: {
    type: Sequelize.STRING(50),
    allowNull: false,
  },

  parent_id: {
    type: Sequelize.INTEGER,
    allowNull: true,
  },
});

module.exports = Categories;
