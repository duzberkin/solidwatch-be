const Sequelize = require("sequelize");
const db = require("../config/db.config");
const Roles = require("./Roles.model");

// Users model
const Users = db.define("users", {
  userName: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  eMail: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  password: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  token: {
    type: Sequelize.STRING,
  },
});

// Assosications
Users.belongsToMany(Roles, {
  through: "User_Roles",
  onDelete: "cascade",
});

Roles.belongsToMany(Users, {
  through: "User_Roles",
  onDelete: "cascade",
});

module.exports = Users;
