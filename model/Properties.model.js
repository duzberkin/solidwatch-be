const { Sequelize } = require("sequelize");
const db = require("../config/db.config");

// Properties Model
const Properties = db.define("properties", {
  name: {
    type: Sequelize.ARRAY(Sequelize.STRING),
    allowNull: false
  },
});

module.exports = Properties;