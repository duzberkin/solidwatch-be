const Sequelize = require("sequelize");
const db = require("../config/db.config");

// Images model
const Images = db.define("images", {
  path: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  type: {
    type: Sequelize.STRING(5),
    allowNull: false,
  },
  size: {
    type: Sequelize.FLOAT,
  },
});

module.exports = Images;