const Sequelize = require("sequelize");
const db = require("../config/db.config");
const Categories = require("./Categories.model");
//const Product_Properties = require("./Product_Properties.model");
const Properties = require("./Properties.model");

// Products Model
const Products = db.define("products", {
  productName: {
    type: Sequelize.STRING(100),
    allowNull: false,
  },
  description: {
    type: Sequelize.TEXT,
    allowNull: true,
  },
  stock: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },
  price: {
    type: Sequelize.FLOAT,
    allowNull: false,
  },
  salePrice: {
    type: Sequelize.FLOAT,
    allowNull: true,
  },
  status: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
  },
  img:{
    type: Sequelize.STRING,
    allowNull:false
  }
});

// Assosications - Database Relations
Products.belongsToMany(Categories, {
  through: "Product_Categories",
  onDelete: "cascade",
});

Categories.belongsToMany(Products, {
  through: "Product_Categories",
  onDelete: "cascade",
});

Products.belongsToMany(Properties, {
  through: "Product_Properties",
  onDelete: "cascade",
});

Properties.belongsToMany(Products, {
  through: "Product_Properties",
  onDelete: "cascade",
});

module.exports = Products;