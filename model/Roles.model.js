const Sequelize = require("sequelize");
const db = require("../config/db.config");

// Roles model
const Roles = db.define("roles", {
  roleName: {
    type: Sequelize.STRING(20),
    allowNull: false,
  },
});

module.exports = Roles;