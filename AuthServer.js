require("dotenv").config();
const bodyParser = require("body-parser");
const express = require("express");
const cors = require("cors");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs/dist/bcrypt");
const userService = require("./service/User.service");
const db = require("./config/db.config");
const Users = require("./model/Users.model");
const app = express();
var LocalStorage = require('node-localstorage').LocalStorage;
var localStorage = new LocalStorage('./scratch');

/**
 * @swagger
 * tags:
 *   name: Auth
 *   description: APIs for auth operations.
 */

// Middlewares
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.options("*", cors());

// DB Authentication
db.authenticate()
  .then(() => {
    console.log("Database connected..");
  })
  .catch((err) => {
    console.log(err);
  });

//let tokenArray = [];
let storage = localStorage.getItem("token");

// Check if token is valid and exists
app.post("/api/v1/token", (req, res) => {
  const refreshToken = req.body.token;
  if (refreshToken == null) return res.sendStatus(401); // Unauthorized
  if (storage !== refreshToken) return res.sendStatus(403); // Forbidden
  jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
    if (err) return res.sendStatus(403); // Forbidden
    const accessToken = generateAccessToken({ eMail: user.eMail });
    res.json({ accessToken: accessToken });
  });
});


/**
 * @swagger
 * /auth/login:
 *   post:
 *     summary: Login
 *     tags: [Auth]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *            schema:
 *               type: object
 *               properties:
 *                       eMail:
 *                         type: string
 *                         description: The user's e-mail.
 *                         example: solid35@solid.com
 *                       password:
 *                         type: string
 *                         description: The user's password.
 *                         example: solid
 *     responses:
 *       200:
 *         description: Login successful

 *       500:
 *         description: Some server error
 */
// Login action
app.post("/api/v1/auth/login", async (req, res) => {
  const user = await userService.getUserByEMail(req.body.eMail);
  if (user === null) {
    return res.status(400).json("Can not find user"); // Bad request
  }
  const dataUser = {
    id: user.id,
    userName: user.userName,
    eMail: user.eMail,
    userrole: user.userrole,
  };
  try {
    if (await bcrypt.compare(req.body.password, user.password)) {
      const accessToken = generateAccessToken(dataUser);
      const refreshToken = jwt.sign(dataUser, process.env.REFRESH_TOKEN_SECRET);

      localStorage.setItem('token', refreshToken);
      Users.update( { token: refreshToken }, { where: { id: user.id } });

      res
        .status(200) // OK
        .json({
          message: "login successfull",
          accessToken: accessToken,
          refreshToken: refreshToken,
        });
    } else {
      res.status(500).json("invalid password or email"); // Internal server error
    }
  } catch (err) {
    console.log(err);
  }
});

// Logout action
app.post("/api/v1/logout", async (req, res) => {
  const user = await userService.getUserByToken(req.body.token);
  Users.update( { token: null }, { where: { id: user.id } });

  res.sendStatus(204); // No content
});

// Generating access token for 5 minutes
function generateAccessToken(user) {
  return jwt.sign(user, process.env.ACCES_TOKEN_SECRET, { expiresIn: "5m" });
}

const PORT = process.env.PORT || 4000;
db.sync()
  .then(() => {
    app.listen(PORT, console.log(`Server started on port ${PORT}`));
  })
  .catch((err) => console.log(err));