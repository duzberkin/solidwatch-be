const Users = require("../model/Users.model");
const jwt_decode = require("jwt-decode");

/**
 * Gets products from API (12 products per page).
 * @function
 * @param {object} req - request object
 * @param {object} res - response object 
 * @param next - Continue execution
 */
let Admin = async (req, res, next) => {
  let token = req.headers.cookie;
  if (token) {
    let decoded = jwt_decode(token, { data: true });
    let userId = decoded.id;

    let role = await Users.findAll({
      attributes: ["userrole"],
      where: { id: userId },
      raw: true,
    });

    if (role[0].userrole == 1) {
      next();
    } else {
      res.status(401).json({ // Unauthorized
        success: false,
        message: "You're not authorized",
      });
    }
  }
};

module.exports = Admin;
