const InvalidIdException = require("../error/InvalidIdException");

/**
 * Invalid or null ID control/check
 * @function
 * @param {object} req - request object
 * @param {object} res - response object 
 * @param next - Continue execution
 */
module.exports = (req, res, next) => {
    const id = Number.parseInt(req.params.id);
    if (Number.isNaN(id)) {
        throw new InvalidIdException();
    }
    next();
}