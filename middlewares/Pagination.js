// Pagination function
module.exports = (req) => {

    const pageAsNumber = Number.parseInt(req.query.page);
    let page = 0;

    if (!Number.isNaN(pageAsNumber) && pageAsNumber > 0) {
        page = pageAsNumber;
    }

    req.pagination = {
        page
    }
}