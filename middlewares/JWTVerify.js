const jwt = require('jsonwebtoken');
require('dotenv').config();

let JWTVerify = { authenticateToken: authenticateToken }

/**
 * JWT token verification
 * @function
 * @param {object} req - request object
 * @param {object} res - response object 
 * @param next - Continue execution
 */
function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];
    if (token === null) return res.status(401).json('Unauthorized'); // Unauthorized

    jwt.verify(token, process.env.ACCES_TOKEN_SECRET, (err, user) => {
        if (err) return res.status(403).json('Forbidden'); // Forbidden
        req.user = user;
        next();
    })
}

module.exports = JWTVerify;