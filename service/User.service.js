const UserNotFoundException = require("../error/UserNotFoundException");
const Roles = require("../model/Roles.model");
const Users = require("../model/Users.model");

// Exporting functions
let userService = {
  getUsers: getUsers,
  getUsersById: getUsersById,
  getUserByEMail: getUserByEMail,
  setUserRole: setUserRole,
  getUserByToken: getUserByToken
};

/**
 * Getting all users
 * @function
 */
async function getUsers(req, res) {
  try {
    return await Users.findAll();
  } catch (err) {
    console.log(err);
  }

  return "Can not get users";
}

/**
 * Getting a specific user by id
 * @function
 * @param {number} Id - Identification of a user you want to filter.
 */
async function getUsersById(id) {
  const user = await Users.findByPk(id);
  if (!user) {
    console.log("asddddddsasd");
    throw new UserNotFoundException();
  }
  return user;
}

/**
 * Getting a specific user by email
 * @function
 * @param {string} Email - Email of a user you want to filter.
 */
async function getUserByEMail(eMail) {
  return await Users.findOne({ where: { eMail: eMail }, raw: true });
}

/**
 * Getting a specific user by token
 * @function
 * @param {string} Token - Token of a user you want to filter.
 */
 async function getUserByToken(token) {
  return await Users.findOne({ where: { token: token }, raw: true });
}

/**
 * Setting a role to user
 * @function
 * @param {number} RoleId - Role identification you want to set.
 * @param {number} UserId - User identification you want to set.
 */
async function setUserRole(roleId, userId) {
  const user = await getUsersById(userId);
  const role = await Roles.findByPk(roleId);
  return await user.setRoles(role);
}

module.exports = userService;
