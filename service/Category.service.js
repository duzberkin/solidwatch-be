const Categories = require("../model/Categories.model");

// Exporting functions
let categoryService = {
  addCategory: addCategory,
  getCategories: getCategories,
  getCategoriesById: getCategoriesById,
};

/**
 * Adding a new category
 * @function
 * @param {object} Category - Category you want to add.
 */
async function addCategory(category) {
  try {
    await Categories.create(category);
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
}

/**
 * Getting all categories
 * @function
 */
async function getCategories() {
  try {
    return await Categories.findAll();
  } catch (error) {
    console.log("Can not get categoris");
  }
}

/**
 * Getting a specific category by id
 * @function
 * @param {number} Id - Identification of a category you want to filter.
 */
async function getCategoriesById(id) {
  try {
    return await Categories.findByPk(id);
  } catch (error) {
    return "Cannot get given category";
  }
}

module.exports = categoryService;
