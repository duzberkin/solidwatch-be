const Categories = require("../model/Categories.model");
const Products = require("../model/Products.model");
const Properties = require("../model/Properties.model");
const { Op } = require("sequelize");

// Exporting functions
let productService = {
  addProduct: addProduct,
  getProducts: getProducts,
  getProductsById: getProductsById,
  getProductByCategory: getProductByCategory,
};

/**
 * Adding a new product
 * @function
 * @param {object} Product - Product you want to add.
 */
async function addProduct(product) {
  let response = true;
  Products.create(product)
    .then((data) => {
      const categories = product.categoryId;
      categories.forEach(async (categoryId) => {
        const category = await Categories.findByPk(categoryId);
        data.addCategories(category);
      });
      const propId = product.propId;
      propId.forEach(async (propertyId) => {
        const property = await Properties.findByPk(propertyId);
        data.addProperties(property);
      });
    })
    .catch((error) => {
      response = false;
      console.log(error);
      return false;
    });
  return response;
}

/**
 * Getting all products (12 per page)
 * @function
 * @param {number} PageNumber - Page as number.
 */
async function getProducts(queryPage) {
  const pageAsNumber = Number.parseInt(queryPage);
  let page = 0;
  if (!Number.isNaN(pageAsNumber) && pageAsNumber > 0) {
    page = pageAsNumber;
  }
  try {
    return await Products.findAndCountAll({
      limit: 12,
      offset: page * 12,
    });
  } catch (error) {
    return "Could not get products";
  }
}

/**
 * Getting a specific product by id
 * @function
 * @param {number} Id - Identification of a product you want to filter.
 */
async function getProductsById(id) {
  try {
    return await Products.findByPk(id, { include: [Categories] });
  } catch (error) {
    return "Could not get product";
  }
}

/**
 * Getting all products by category
 * @function
 * @param {number} Id - Identification of a product you want to filter.
 * @param {number} PageNumber - Page as number.
 */
async function getProductByCategory(id, queryPage) {
  const pageAsNumber = Number.parseInt(queryPage);
  let page = 0;
  if (!Number.isNaN(pageAsNumber) && pageAsNumber > 0) {
    page = pageAsNumber;
  }

  try {
    const products = await Products.findAndCountAll({
      limit: 12,
      offset: page * 12,
      include: [
        {
          model: Categories,
          where: { [Op.or]: [{ id: id }, { parent_id: id }] },
        },
      ],
    });
    return products;
  } catch (error) {
    return "Could not get the products for the specified category";
  }
}

module.exports = productService;
