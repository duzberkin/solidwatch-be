const Properties = require("../model/Properties.model");

// Exporting functions
let propertyService = {
  addProperty: addProperty,
  getProperties: getProperties,
  getPropertiesById: getPropertiesById,
};

/**
 * Adding a new property
 * @function
 * @param {object} Property - Property you want to add.
 */
async function addProperty(property) {
  try {
    await Properties.create(property);
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
}

/**
 * Getting all properties
 * @function
 */
async function getProperties() {
  try {
    return await Properties.findAll();
  } catch (error) {
    console.log("Can not get properties");
  }
}

/**
 * Getting a specific property by id
 * @function
 * @param {number} Id - Identification of a property you want to filter.
 */
async function getPropertiesById(id) {
  try {
    return await Properties.findByPk(id);
  } catch (error) {
    return "Cannot get given property";
  }
}

module.exports = propertyService;