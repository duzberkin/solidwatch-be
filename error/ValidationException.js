// Validation Exception (Invalid Request)
module.exports = function ValidationException(errors) {
    this.status = 422;
    this.message = 'Invalid Request';
    this.errors = errors;
}