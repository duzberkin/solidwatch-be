const express = require("express");
const router = express.Router();
const productRoutes = require("./product.route");
const categoryRoutes = require("./category.route");
const roleRoutes = require("./role.route");
const userRoutes = require("./user.routes");
const propertyRoutes = require("../routes/properties.route");
const logout = require("../controller/User.logout");

// Root routes for all models
router.use("/api/v1/products", productRoutes);
router.use("/api/v1/categories", categoryRoutes);
router.use("/api/v1/roles", roleRoutes);
router.use("/api/v1/users", userRoutes);
router.use("/api/v1/properties", propertyRoutes);
router.use("/api/v1/logout", logout.userLogout);

module.exports = router;
