const express = require("express");
const router = express.Router();
const productController = require("../controller/Product.controlller");
const { authenticateToken } = require("../middlewares/JWTVerify");
const IDControl = require("../middlewares/IDControl");

/**
 * @swagger
 * tags:
 *   name: Product
 *   description: APIs to handle product resources.
 */

// Routes for product model

/**
 *
 * @swagger
 * /products:
 *   get:
 *     summary: Retrieve a list of Solid Watch products.
 *     description: Retrieve a list of products from Solid Watch.
 *     tags: [Product]
 *     responses:
 *       200:
 *         description: A list of products.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                      id:
 *                         type: integer
 *                         description: The product ID.
 *                         example: 1
 *                      productName:
 *                         type: string
 *                         description: The product's name.
 *                         example: Solid Venom
 *                      description:
 *                         type: string
 *                         description: The product's description.
 *                         example: Lorem Ipsum is simply dummy text of the printing and typesetting industry.
 *                      stock:
 *                         type: integer
 *                         description: The product's stock.
 *                         example: Lorem Ipsum is simply dummy text of the printing and typesetting industry.
 *                      price:
 *                         type: float
 *                         description: The product's price.
 *                         example: 100
 *                      salePrice:
 *                         type: float
 *                         description: The product's sale price.
 *                         example: 370
 *                      status:
 *                         type: boolean
 *                         description: Is product available?
 *                         example: true
 *                      img:
 *                         type: string
 *                         description: Image path of product
 *                         example: https://res.cloudinary.com/ddrfezrwp/image/upload/v1641133080/assets/assets/men/casual/solid_venom_y7jmdd.webp
 *                      createdAt:
 *                         type: date
 *                         description: Creation date of product.
 *                         example: 2022-01-04T09:16:27.214Z
 *                      updatedAt:
 *                         type: string
 *                         description: Update date of product.
 *                         example: 2022-01-04T09:16:27.214Z
 *
 */
router.get("/", productController.getProducts);

/**
 * @swagger
 * /products/{id}:
 *   get:
 *     summary: Retrieve a single Solid Watch product.
 *     description: Retrieve a single Solid Watch product.
 *     tags: [Product]
 *     parameters:
 *        - in : path
 *          name: id
 *          description: id of product
 *     responses:
 *       200:
 *         description: A single product.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                      id:
 *                         type: integer
 *                         description: The product ID.
 *                         example: 1
 *                      productName:
 *                         type: string
 *                         description: The product's name.
 *                         example: Solid Venom
 *                      description:
 *                         type: string
 *                         description: The product's description.
 *                         example: Lorem Ipsum is simply dummy text of the printing and typesetting industry.
 *                      stock:
 *                         type: integer
 *                         description: The product's stock.
 *                         example: Lorem Ipsum is simply dummy text of the printing and typesetting industry.
 *                      price:
 *                         type: float
 *                         description: The product's price.
 *                         example: 100
 *                      salePrice:
 *                         type: float
 *                         description: The product's sale price.
 *                         example: 370
 *                      status:
 *                         type: boolean
 *                         description: Is product available?
 *                         example: true
 *                      img:
 *                         type: string
 *                         description: Image path of product
 *                         example: https://res.cloudinary.com/ddrfezrwp/image/upload/v1641133080/assets/assets/men/casual/solid_venom_y7jmdd.webp
 *                      createdAt:
 *                         type: date
 *                         description: Creation date of product.
 *                         example: 2022-01-04T09:16:27.214Z
 *                      updatedAt:
 *                         type: string
 *                         description: Update date of product.
 *                         example: 2022-01-04T09:16:27.214Z
 *
 */
router.get("/:id", IDControl, productController.getProductsById);

/**
 * @swagger
 * /products:
 *   post:
 *     summary: Add a new product.
 *     tags: [Product]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *            schema:
 *               type: object
 *               properties:
 *                       productName:
 *                         type: string
 *                         description: New product's name.
 *                         example: Venom
 *                       description:
 *                         type: string
 *                         description: New product's description.
 *                         example: Elite, spectacular and cheap
 *                       stock:
 *                         type: integer
 *                         description: New product's stock.
 *                         example: 20
 *                       price:
 *                         type: float
 *                         description: New product's price.
 *                         example: 300
 *                       salePrice:
 *                         type: float
 *                         description: New product's price.
 *                         example: 270
 *                       categoryId:
 *                         type: array
 *                         description: New product's categories.
 *                         example: [1,3]
 *     responses:
 *       200:
 *         description: The product was successfully added

 *       500:
 *         description: Some server error
 */
router.post("/", authenticateToken, productController.addProduct);

/**
 * @swagger
 * /products/by-category/{id}:
 *   get:
 *     summary: List of products in a category with category id.
 *     description: Retrieve products in a category with category id.
 *     tags: [Product]
 *     parameters:
 *        - in : path
 *          name: id
 *          description: id of category
 *     responses:
 *       200:
 *         description: Products in a category.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                      id:
 *                         type: integer
 *                         description: The product ID.
 *                         example: 1
 *                      productName:
 *                         type: string
 *                         description: The product's name.
 *                         example: Solid Venom
 *                      description:
 *                         type: string
 *                         description: The product's description.
 *                         example: Lorem Ipsum is simply dummy text of the printing and typesetting industry.
 *                      stock:
 *                         type: integer
 *                         description: The product's stock.
 *                         example: Lorem Ipsum is simply dummy text of the printing and typesetting industry.
 *                      price:
 *                         type: float
 *                         description: The product's price.
 *                         example: 100
 *                      salePrice:
 *                         type: float
 *                         description: The product's sale price.
 *                         example: 370
 *                      status:
 *                         type: boolean
 *                         description: Is product available?
 *                         example: true
 *                      img:
 *                         type: string
 *                         description: Image path of product
 *                         example: https://res.cloudinary.com/ddrfezrwp/image/upload/v1641133080/assets/assets/men/casual/solid_venom_y7jmdd.webp
 *                      createdAt:
 *                         type: date
 *                         description: Creation date of product.
 *                         example: 2022-01-04T09:16:27.214Z
 *                      updatedAt:
 *                         type: string
 *                         description: Update date of product.
 *                         example: 2022-01-04T09:16:27.214Z
 *
 *
 */
router.get(
  "/by-category/:id",
  IDControl,
  productController.getProductByCategory
);

module.exports = router;
