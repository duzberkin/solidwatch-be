const propertyController = require("../controller/Property.controller");
const express = require("express");
const router = express.Router();

/**
 * @swagger
 * tags:
 *   name: Property
 *   description: APIs to handle properties.
 */


// Routes for properties model


/**
 *
 * @swagger
 * /properties:
 *   get:
 *     summary: Retrieve a list of Solid Watch properties.
 *     description: Retrieve a list of properties from Solid Watch.
 *     tags: [Property]
 *     responses:
 *       200:
 *         description: A list of properties.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       id:
 *                         type: integer
 *                         description: The property ID.
 *                         example: 1
 *                       name:
 *                         type: array
 *                         description: Name of property.
 *                         example: ["color","gold"]
 *                       createdAt:
 *                         type: date
 *                         description: Creation date of property.
 *                         example: 2022-01-04T09:16:27.214Z
 *                       updatedAt:
 *                         type: date
 *                         description: Update date of property.
 *                         example: 2022-01-04T09:16:27.214Z
 *
 */
router.get("/", propertyController.getProperties);


/**
 * @swagger
 * /properties/{id}:
 *   get:
 *     summary: Retrieve a single Solid Watch property.
 *     description: Retrieve a single Solid Watch property.
 *     tags: [Property]
 *     parameters:
 *        - in : path
 *          name: id
 *          description: id of property
 *     responses:
 *        200:
 *          description: A list of properties.
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  data:
 *                    type: array
 *                    items:
 *                      type: object
 *                      properties:
 *                        id:
 *                          type: integer
 *                          description: The property ID.
 *                          example: 1
 *                        name:
 *                          type: array
 *                          description: Name of property.
 *                          example: ["color","gold"]
 *                        createdAt:
 *                          type: date
 *                          description: Creation date of property.
 *                          example: 2022-01-04T09:16:27.214Z
 *                        updatedAt:
 *                          type: date
 *                          description: Update date of property.
 *                          example: 2022-01-04T09:16:27.214Z
 *
 *
 */
router.get("/:id", propertyController.getPropertiesById);



/**
 * @swagger
 * /properties:
 *   post:
 *     summary: Create a new property.
 *     tags: [Property]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *            schema:
 *               type: object
 *               properties:
 *                       name:
 *                         type: array
 *                         description: Create a new property name
 *                         example: ["color","gold"]
 *     responses:
 *       200:
 *         description: The property was successfully created

 *       500:
 *         description: Some server error
 */
router.post("/", propertyController.addProperty);


module.exports = router;