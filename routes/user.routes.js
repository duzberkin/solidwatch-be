const express = require("express");
const router = express.Router();
const userController = require("../controller/User.controller");
const register = require("../controller/user.register");
const IDControl = require("../middlewares/IDControl");
const { validateUser } = require("../validation/user_validation/UserRegValid");

/**
 * @swagger
 * tags:
 *   name: User
 *   description: APIs to handle user resources.
 */

// Routes for registration and login

/**
 *
 * @swagger
 * /users:
 *   get:
 *     summary: Retrieve a list of Solid Watch users.
 *     description: Retrieve a list of users from Solid Watch.
 *     tags: [User]
 *     responses:
 *       200:
 *         description: A list of users.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       id:
 *                         type: integer
 *                         description: The user ID.
 *                         example: 1
 *                       userName:
 *                         type: string
 *                         description: The user's username.
 *                         example: osmanali
 *                       email:
 *                         type: string
 *                         description: The user's username.
 *                         example: osmanali@mail.com
 *                       security:
 *                        - jwt: []
 *
 */
router.get("/", userController.getUsers);

/**
 * @swagger
 * /users/{id}:
 *   get:
 *     summary: Retrieve a single Solid Watch user.
 *     description: Retrieve a single Solid Watch user.
 *     tags: [User]
 *     parameters:
 *        - in : path
 *          name: id
 *          description: id of user
 *     responses:
 *       200:
 *         description: A single user.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: object
 *                   properties:
 *                     id:
 *                       type: integer
 *                       description: The user ID.
 *                       example: 1
 *                     name:
 *                       type: string
 *                       description: The user's name.
 *                       example: Leanne Graham
 */
router.get("/:id", IDControl, userController.getUsersById);

/**
 * @swagger
 * /users/register:
 *   post:
 *     summary: Create a new user
 *     tags: [User]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *            schema:
 *               type: object
 *               properties:
 *                       userName:
 *                         type: string
 *                         description: The new user's userName.
 *                         example: solid35
 *                       eMail:
 *                         type: string
 *                         description: The new user's email.
 *                         example: solid@solid.com
 *                       password:
 *                         type: string
 *                         description: The user's password.
 *                         example: solid
 *     responses:
 *       200:
 *         description: The user was successfully created

 *       500:
 *         description: Some server error
 */
router.post("/register", validateUser, register.addUser);

/**
 * @swagger
 * /users/role:
 *   post:
 *     summary: Assign role to user.
 *     tags: [User]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *            schema:
 *               type: object
 *               properties:
 *                       roleId:
 *                         type: integer
 *                         description: Role id for user.
 *                         example: 1
 *                       userId:
 *                         type: integer
 *                         description: The user that assigned the role.
 *                         example: 1
 *     responses:
 *       200:
 *         description: The user was successfully created

 *       500:
 *         description: Some server error
 */
router.post("/role", userController.setUserRole);

module.exports = router;
