const express = require("express");
const router = express.Router();
const rolesController = require("../controller/Role.controller");


/**
 * @swagger
 * tags:
 *   name: Role
 *   description: APIs to handle roles.
 */

// Routes for roles model
/**
 * @swagger
 * /roles:
 *   post:
 *     summary: Create a new role.
 *     tags: [Role]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *            schema:
 *               type: object
 *               properties:
 *                       roleName:
 *                         type: string
 *                         description: Create a new role name
 *                         example: Admin
 *     responses:
 *       200:
 *         description: Role created.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: object
 *                   properties:
 *                       id:
 *                         type: integer
 *                         description: The role ID.
 *                         example: 1
 *                       roleName:
 *                         type: string
 *                         description: Name of role.
 *                         example: Superadmin
 *                       createdAt:
 *                         type: string
 *                         description: Creation date of role.
 *                         example: 2022-01-04T09:16:27.214Z
 *                       updatedAt:
 *                         type: string
 *                         description: Update date of role.
 *                         example: 2022-01-04T09:16:27.214Z
 *
 *
 *       500:
 *         description: Some server error
 */
router.post("/", rolesController.addRole);


/**
 *
 * @swagger
 * /roles:
 *   get:
 *     summary: Retrieve a list of Solid Watch roles.
 *     description: Retrieve a list of roles from Solid Watch.
 *     tags: [Role]
 *     responses:
 *       200:
 *         description: A list of roles.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       id:
 *                         type: integer
 *                         description: The role ID.
 *                         example: 1
 *                       roleName:
 *                         type: string
 *                         description: Name of role.
 *                         example: Superadmin
 *                       createdAt:
 *                         type: string
 *                         description: Creation date of role.
 *                         example: 2022-01-04T09:16:27.214Z
 *                       updatedAt:
 *                         type: string
 *                         description: Update date of role.
 *                         example: 2022-01-04T09:16:27.214Z
 */
router.get("/", rolesController.getRoles);

/**
 * @swagger
 * /roles/{id}:
 *   get:
 *     summary: Retrieve a single user role.
 *     tags: [Role]
 *     parameters:
 *        - in : path
 *          name: id
 *          description: id of role
 *     responses:
 *       200:
 *         description: A single role.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       id:
 *                         type: integer
 *                         description: The role ID.
 *                         example: 1
 *                       roleName:
 *                         type: string
 *                         description: Name of role.
 *                         example: Superadmin
 *                       createdAt:
 *                         type: string
 *                         description: Creation date of role.
 *                         example: 2022-01-04T09:16:27.214Z
 *                       updatedAt:
 *                         type: string
 *                         description: Update date of role.
 *                         example: 2022-01-04T09:16:27.214Z
 */
router.get("/:id", rolesController.getRolesById);

module.exports = router;