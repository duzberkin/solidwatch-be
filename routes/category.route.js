const express = require("express");
const router = express.Router();
const categoryController = require("../controller/Category.controller");

/**
 * @swagger
 * tags:
 *   name: Category
 *   description: APIs to handle categories.
 */

// Routes for category model
/**
 *
 * @swagger
 * /categories:
 *   get:
 *     summary: Retrieve a list of Solid Watch categories.
 *     description: Retrieve a list of categories from Solid Watch.
 *     tags: [Category]
 *     responses:
 *       200:
 *         description: A list of categories.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       id:
 *                         type: integer
 *                         description: The category ID.
 *                         example: 1
 *                       categoryName:
 *                         type: string
 *                         description: Name of category.
 *                         example: Watch
 *                       parent_id:
 *                         type: integer
 *                         description: Parent category id of category. If value null, it is root category.
 *                         example: 1
 *                       createdAt:
 *                         type: date
 *                         description: Creation date of category.
 *                         example: 2022-01-04T09:16:27.214Z
 *                       updatedAt:
 *                         type: date
 *                         description: Update date of category.
 *                         example: 2022-01-04T09:16:27.214Z
 *
 */
router.get("/", categoryController.getCategories);

/**
 * @swagger
 * /categories/{id}:
 *   get:
 *     summary: Retrieve a single Solid Watch category.
 *     description: Retrieve a single Solid Watch category.
 *     tags: [Category]
 *     parameters:
 *        - in : path
 *          name: id
 *          description: id of category
 *     responses:
 *       200:
 *         description: A single category.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: object
 *                   properties:
 *                       id:
 *                         type: integer
 *                         description: The category ID.
 *                         example: 1
 *                       categoryName:
 *                         type: string
 *                         description: Name of category.
 *                         example: Watch
 *                       parent_id:
 *                         type: integer
 *                         description: Parent category id of category. If value null, it is root category.
 *                         example: 1
 *                       createdAt:
 *                         type: string
 *                         description: Creation date of category.
 *                         example: 2022-01-04T09:16:27.214Z
 *                       updatedAt:
 *                         type: string
 *                         description: Update date of category.
 *                         example: 2022-01-04T09:16:27.214Z
 *
 */
router.get("/:id", categoryController.getCategoriesById);

/**
 * @swagger
 * /categories:
 *   post:
 *     summary: Create a new category.
 *     tags: [Category]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *            schema:
 *               type: object
 *               properties:
 *                       categoryName:
 *                         type: string
 *                         description: Create a new category name
 *                         example: Casual
 *     responses:
 *       200:
 *         description: The user was successfully created

 *       500:
 *         description: Some server error
 */
router.post("/", categoryController.addCategory);

module.exports = router;
