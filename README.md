# sw-backend

## Structure

There are database configuration in config folder
Creation of models in model folder.
Control (get/post) in controller folder
Specific routes for all models and controllers in Routes folder

## How it Works

You create new model with **Sequelize**, while you're doing this you use db configuration which define in config file.
You should create get and post methods in controller folder for your models
For use those methods, you should create routes folder for give them specific path.

### Completed Parts

Created Products, Categories, Roles and Users models.
In controller, all models get and post methods created, routes completed.
Register structure completed, with bcrypt given passwords hashed and saved to database.

## Left?

Relative db architecture
Login page & Token creation with JWT which installed.
Creating dataset and implament to database.
Front-end part. - Fetching data, Login/register forms.
Nginx configuration for apis.
...
