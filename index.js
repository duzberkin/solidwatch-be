const bodyParser = require("body-parser");
const express = require("express");
const cors = require("cors");
const db = require("./config/db.config");
const ErrorHandler = require("./error/ErrorHandler");
const app = express();
const categories = require("./datasets/category.json");
const properties = require("./datasets/property.json");
const products = require("./datasets/products.json");
const categoryService = require("./service/Category.service");
const propertyService = require("./service/Property.service");
const productService = require("./service/Product.service");
const swaggerJSDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");

const swaggerDefinition = {
  openapi: "3.0.0",
  info: {
    title: "Express API for Solid Watch",
    version: "1.0.0",
    description:
      "This is a REST API application made with Express. It retrieves data from Solid Watch.",
    license: {
      name: "Licensed Under MIT",
      url: "https://spdx.org/licenses/MIT.html",
    },
    contact: {
      name: "Solid Watch",
      url: "https://solid.emakina.com",
    },
  },
  servers: [
    {
      url: "http://localhost:5000/api/v1",
      description: "Smart Watch server",
    },
    {
      url: "http://localhost:4000/api/v1",
      description: "Smart Watch Auth server",
    },
  ],
  components: {
    securitySchemes: {
      jwt: {
        type: "http",
        scheme: "bearer",
        in: "header",
        bearerFormat: "JWT",
      },
    },
  },
  security: [
    {
      jwt: [],
    },
  ],
};

// DB authentication
db.authenticate()
  .then(() => {
    console.log("Database connected..");
  })
  .catch((err) => {
    console.log(err);
  });

//Middlewares
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.options("*", cors());

//Routes
app.use("/", require("./routes/index"));
app.use(ErrorHandler);

const options = {
  swaggerDefinition,
  // Paths to files containing OpenAPI definitions
  apis: ["./routes/*.js", "./AuthServer.js"],
};
const swaggerSpec = swaggerJSDoc(options);
app.use("/docs", swaggerUi.serve, swaggerUi.setup(swaggerSpec));

const PORT = process.env.PORT || 5500;
db.sync()
  .then(() => {
    // categories.forEach((data) => {
    //   categoryService.addCategory(data);
    // });
    // properties.forEach((data) => {
    //   propertyService.addProperty(data);
    // });
    // products.forEach((data) => {
    //   productService.addProduct(data);
    // });
    // res.send(200).json("Database Loaded");
    app.listen(PORT, console.log(`Server started on port ${PORT}`));
  })
  .catch((err) => console.log(err));
