/*
 ** Users Control File
 */

const userService = require('../service/User.service');

// Exporting functions
let usersController = {
  getUsers: getUsers,
  getUsersById: getUsersById,
  setUserRole: setUserRole
};

// Getting all users
async function getUsers(req, res) {
  let users = await userService.getUsers();
  res.send(users);
}

// Getting a specific user by id
async function getUsersById(req, res, next) {
  try {
    const user = await userService.getUsersById(req.params.id);
    res.send(user);
  } catch (error) {
    next(error);
  }
}

// Setting a role to user
async function setUserRole(req, res) {
  userService.setUserRole(req.body.roleId, req.body.userId);
  res.status(200).json({ // OK
    success: true,
    message: "User role has been successfully set.",
  });
}

module.exports = usersController;