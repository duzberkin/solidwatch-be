/*
 ** User Logout File
 */

const Users = require("../model/Users.model");
const jwt_decode = require("jwt-decode");

// Exporting functions
let logout = {
  userLogout: userLogout,
};

// Logout function
async function userLogout(req, res) {
  let cookies = req.cookies["auth"];
  console.log("Token: ", cookies);
  let decoded = jwt_decode(cookies, { data: true });
  let userId = decoded.id;

  Users.update({ token: "" }, { where: { id: userId } });
  res.clearCookie("auth");
  res.status(200).json({ // OK
    success: true,
    message: "Logout completed.",
  });
}

module.exports = logout;