/*
 ** Category Model Control File
 */
const categoryService = require('../service/Category.service');

// Exporting functions
let categoryController = {
  addCategory: addCategory,
  getCategories: getCategories,
  getCategoriesById: getCategoriesById,
};

// Creating a new category
async function addCategory(req, res) {
  let response = await categoryService.addCategory(req.body);
  if (response) {
    res.status(201).json('Category created'); // Successfully created
  }
  else {
    res.status(500).json('Category could not be created'); // Internal server error
  }
}

// Get all categories
async function getCategories(req, res) {
  const categories = await categoryService.getCategories();
  res.status(200).send(categories); // OK
}

// Get a spesific category by id
async function getCategoriesById(req, res) {
  const category = await categoryService.getCategoriesById(req.params.id);
  if (category) {
    res.status(200).send(category); // OK
  } else {
    res.json("Can not find category");
  }
}

module.exports = categoryController;