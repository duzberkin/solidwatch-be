/*
 ** Property Model Control File
 */
const propertyService = require("../service/Property.service");

// Exporting functions
let propertyController = {
  addProperty: addProperty,
  getProperties: getProperties,
  getPropertiesById: getPropertiesById,
};

// Creating a new category
async function addProperty(req, res) {
  let response = await propertyService.addProperty(req.body);
  if (response) {
    res.status(201).json("Property created"); // Successfully created
  } else {
    res.status(500).json("Property could not be created"); // Internal server error
  }
}

// Get all properties
async function getProperties(req, res) {
  const property = await propertyService.getProperties();
  res.status(200).send(property); // OK
}

// Get a spesific property
async function getPropertiesById(req, res) {
  const property = await propertyService.getPropertiesById(req.params.id);
  if (property) {
    res.status(200).send(property); // OK
  } else {
    res.json("Can not find property");
  }
}

module.exports = propertyController;