/*
 ** Products Control File
 */

const productService = require("../service/Product.service");

// Exporting functions
let productController = {
  addProduct: addProduct,
  getProducts: getProducts,
  getProductsById: getProductsById,
  getProductByCategory: getProductByCategory,
};

// Create new product
async function addProduct(req, res) {
  const response = await productService.addProduct(req.body);
  if (response) {
    res.status(200).json("product created"); // OK
  } else {
    res.status(500).json("product can not be created"); // Internal server error
  }
}

// Get all products
async function getProducts(req, res) {
  const products = await productService.getProducts(req.query.page);
  res.status(200).send({ // OK
    content: products.rows,
    totalPage: Math.ceil(products.count / 12)
  });
}

// Get a spesific product by id
async function getProductsById(req, res) {
  const product = await productService.getProductsById(req.params.id);
  res.status(200).send(product); // OK
}

// Get products by category
async function getProductByCategory(req, res) {
  const products = await productService.getProductByCategory(req.params.id, req.query.page);
  res.status(200).send({ // OK
    content: products.rows,
    totalPage: Math.ceil(products.count / 12)
  });
}

module.exports = productController;