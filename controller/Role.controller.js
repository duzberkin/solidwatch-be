/*
 ** Roles Control File
 */

const Roles = require("../model/Roles.model");

// Exporting functions
let rolesController = {
  addRole: addRole,
  getRoles: getRoles,
  getRolesById: getRolesById,
};

// Creating a new role model
function addRole(req, res) {
  Roles.create(req.body)
    .then((data) => {
      res.send(data);
    })
    .catch((error) => {
      console.log(error);
    });
}

// Get all roles
function getRoles(req, res) {
  Roles.findAll()
    .then((data) => {
      res.send(data);
    })
    .catch((error) => console.log(error));
}

// Get a spesific role
function getRolesById(req, res) {
  Roles.findByPk(req.params.id)
    .then((data) => {
      res.send(data);
    })
    .catch((error) => {
      console.log(error);
    });
}

module.exports = rolesController;
