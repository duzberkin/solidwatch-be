/*
 ** User Register File
 */

const bcrypt = require("bcryptjs/dist/bcrypt");
const Roles = require("../model/Roles.model");
const Users = require("../model/Users.model");

// Exporting functions
let register = {
  addUser: addUser,
};

// Creating a new user
async function addUser(req, res) {
  try {
    const salt = await bcrypt.genSalt();
    const hashedPassword = await bcrypt.hash(req.body.password, salt);
    const user = {
      userName: req.body.userName,
      eMail: req.body.eMail,
      password: hashedPassword,
    };
    Users.create(user)
      .then(async (data) => {
        const defRole = await Roles.findByPk(3);
        data.addRoles(defRole);
        res.status(201).json("user created"); // Created
      })
      .catch((error) => {
        console.log(error);
        res.status(500).json("user can not be created"); // Internal server error
      });
  } catch (error) {
    console.log(error);
    res.status(500).json("user can not be created"); // Internal server error
  }
}

module.exports = register;
