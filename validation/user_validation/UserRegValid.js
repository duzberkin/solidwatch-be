const { body, validationResult } = require('express-validator');
const ValidationException = require('../../error/ValidationException');
const userService = require('../../service/User.service');

// Regex validation for username and mail
exports.validateUser = [
  body('userName').notEmpty()
    .withMessage('Username cannot be null')
    .bail()
    .isLength({ min: 4, max: 10 }).withMessage('Username must have min 4 and max 10 characters'),
  body('eMail').isEmail().withMessage('Must be a valid e-mail address')
    .custom(async (email) => {
      const user = await userService.getUserByEMail(email);
      console.log(user);
      if (user) {
        throw new Error('This email is already in use');
      }
    }),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty())
      return next(new ValidationException(errors.array()));
    next();
  },
];